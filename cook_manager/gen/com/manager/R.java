/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.manager;

public final class R {
    public static final class attr {
    }
    public static final class color {
        public static final int black=0x7f040004;
        public static final int cook_color_bg=0x7f040000;
        public static final int cooked=0x7f040006;
        public static final int gray=0x7f040003;
        public static final int login_bg_color=0x7f040007;
        public static final int login_toRegister_tips=0x7f040008;
        public static final int order_time_font=0x7f040005;
        public static final int orders_header_text_color=0x7f040001;
        public static final int transparent=0x7f040009;
        public static final int white=0x7f040002;
    }
    public static final class drawable {
        public static final int cook_circle_corner_bottom=0x7f020000;
        public static final int cook_circle_corner_middle=0x7f020001;
        public static final int cook_circle_corner_top=0x7f020002;
        public static final int cook_header_bg=0x7f020003;
        public static final int cook_loading_start=0x7f020004;
        public static final int cook_loading_start__=0x7f020005;
        public static final int cook_refresh=0x7f020006;
        public static final int cook_textview_delete_btn=0x7f020007;
        public static final int cook_wrong=0x7f020008;
        public static final int ic_launcher=0x7f020009;
        public static final int login_btn_bg=0x7f02000a;
        public static final int notice_icon=0x7f02000b;
        public static final int order_call_user_bg=0x7f02000c;
        public static final int order_icon_contracted=0x7f02000d;
        public static final int order_icon_deliveried=0x7f02000e;
        public static final int order_icon_expanded=0x7f02000f;
        public static final int order_mid_bg=0x7f020010;
        public static final int order_selector=0x7f020011;
        public static final int order_white=0x7f020012;
    }
    public static final class id {
        public static final int address_bar_refreshing_text=0x7f08000b;
        public static final int cook_loading_progressBar=0x7f080000;
        public static final int cook_loading_text=0x7f080001;
        public static final int login_btn=0x7f080006;
        public static final int login_clear_password=0x7f080005;
        public static final int login_clear_username=0x7f080003;
        public static final int login_password=0x7f080004;
        public static final int login_toRegister_tips=0x7f080007;
        public static final int login_username=0x7f080002;
        public static final int menu_list=0x7f080015;
        public static final int menu_list_container=0x7f080014;
        public static final int menu_settings=0x7f080027;
        public static final int order_call_receiver=0x7f08001e;
        public static final int order_call_receiver_container=0x7f08001d;
        public static final int order_head=0x7f080008;
        public static final int order_history=0x7f08000c;
        public static final int order_icon_status=0x7f080013;
        public static final int order_item=0x7f08000f;
        public static final int order_item_indicator=0x7f080025;
        public static final int order_item_parent=0x7f080023;
        public static final int order_item_parent_text=0x7f080024;
        public static final int order_item_rest=0x7f080010;
        public static final int order_item_time=0x7f080011;
        public static final int order_menu_name=0x7f080020;
        public static final int order_menu_num=0x7f080022;
        public static final int order_menu_price=0x7f080021;
        public static final int order_num=0x7f080026;
        public static final int order_receiverAddress=0x7f080019;
        public static final int order_receiverAddress_text=0x7f08001a;
        public static final int order_refreshed=0x7f08000a;
        public static final int order_refreshing=0x7f080009;
        public static final int order_remark=0x7f08001b;
        public static final int order_remark_text=0x7f08001c;
        public static final int order_secret=0x7f080012;
        public static final int order_setCloseOpen=0x7f08000e;
        public static final int order_setDeliveried=0x7f08001f;
        public static final int order_setTime=0x7f08000d;
        public static final int order_totalPrice=0x7f080016;
        public static final int order_totalPrice_text=0x7f080018;
        public static final int order_totalPrice_text_pre=0x7f080017;
    }
    public static final class layout {
        public static final int cook_loading=0x7f030000;
        public static final int cook_loading_onstart=0x7f030001;
        public static final int login=0x7f030002;
        public static final int order=0x7f030003;
        public static final int order_item_child=0x7f030004;
        public static final int order_item_menu=0x7f030005;
        public static final int order_item_parent=0x7f030006;
        public static final int order_menu=0x7f030007;
    }
    public static final class menu {
        public static final int polling=0x7f070000;
    }
    public static final class string {
        public static final int app_name=0x7f050000;
        public static final int hello_world=0x7f050001;
        public static final int menu_settings=0x7f050002;
        public static final int order_top_text=0x7f050003;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
}
