/**
 * 
 */
package com.lurenshuo.zhanggui.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lurenshuo.zhanggui.activity.LoginActivity;
import com.manager.R;

/**@description ��ͼ��Ĺ�����
 * @author ������
 *
 * 2013-4-24 ����10:24:40
 */
public class ViewUtils {

	/**
	 * �������ڼ����е���ʾ��,����ʱ��ȷ����ʾ��ʱ��
	 * @param loginActivity
	 * @param cookLoading
	 * @param textҪ��ʾ��dialog����Ϣ
	 * @return
	 */
	public static AlertDialog alertLoading(Context context, String text) {

		final AlertDialog dlg = new AlertDialog.Builder(context).create();
		dlg.show();
		Window window = dlg.getWindow();
		window.setContentView(R.layout.cook_loading);
		TextView tv = (TextView) window.findViewById(R.id.cook_loading_text);
		tv.setText(text);
		
		return dlg;
	}

	/**
	 * ����û�����һ���ؼ��Ժ���ʾ��һ���ؼ��ļ���;
	 * �û��뿪��һ���ؼ��Ժ�������һ���ؼ��ļ���
	 * @param view_touch
	 * @param view_visible
	 */
	public static void setView_VisibleOrInVisibleListener(final EditText view_touch,
			final View view_visible_invisible) {
		
		view_touch.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				
				if(hasFocus){
						
					view_visible_invisible.setVisibility(View.VISIBLE);
				}else {
					
					view_visible_invisible.setVisibility(View.INVISIBLE);
				}
			}
		});
	}

	/**����ÿ�EditText�ļ���
	 * @param phoneET
	 * @param clearPhoneButton
	 */
	public static void setViewEmpty(final EditText phoneET,
			View clearPhoneButton) {
		
		clearPhoneButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				phoneET.setText("");
			}
		});
	}
	
	
	/**�����쳣��ת����ҳ��activity
	 * @param handler
	 * @param context
	 */
	public static void handleNetException_toLoginActivity(Handler handler,final Activity context,final String content){
		
		handler.post(new Runnable() {
			@Override
			public void run() {
				new AlertDialog.Builder(context).setIcon(R.drawable.cook_wrong).setTitle("��������쳣").setMessage(content).setPositiveButton("ȷ��", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent();
						intent.setClass(context, LoginActivity.class);
						context.startActivity(intent);
					}
				}).show();
			}
		});
	}
	
	/**�����쳣��ת��ѡ��ַҳ��activity
	 * @param handler
	 * @param context
	 */
	public static void handleNetException_toAddressActivity(Handler handler,final Activity context,final String content){
		
//		handler.post(new Runnable() {
//			@Override
//			public void run() {
//				new AlertDialog.Builder(context).setIcon(R.drawable.cook_wrong).setTitle("��������쳣").setMessage(content).setPositiveButton("ȷ��", new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						Intent intent = new Intent();
//						intent.setClass(context, AddressActivity.class);
//						context.startActivity(intent);
//					}
//				}).show();
//			}
//		});
	}
	
	/**�����쳣����ʾ�û����ݼ���ʧ��,����ת
	 * @param handler
	 * @param context
	 */
	public static void handleNetException_alert_noSkip(Handler handler,final Activity context,final String content){
		
		handler.post(new Runnable() {
			@Override
			public void run() {
				
				new AlertDialog.Builder(context).setIcon(R.drawable.cook_wrong).setTitle("��������쳣").setMessage(content).setPositiveButton("ȷ��", null).show();
			}
		});
	}
	
	/**�����쳣����Toast��ʾ�û��޷���������
	 * @param handler
	 * @param context
	 */
	public static void handleNetException_alert_byToast(Handler handler,final Activity context,final String content){
		
		handler.post(new Runnable() {
			@Override
			public void run() {
				
				Toast.makeText(context, content, Toast.LENGTH_LONG).show();
			}
		});
	}
	

	/**��ʼ��ȫ�ֱ�����ֵ����գ�
	 * @param context
	 */
	public static void initAppContext(Activity context){
		
//		AppContext appContext = (AppContext) context.getApplication();
//		appContext.setOrderList(null);
//		appContext.setRestaurantMap(null);
//		appContext.setRestInfoList(null);
	}
	/**��ʼ��ȫ�ֱ�����ֵ
	 * @param context
	 */
	public static void initAppContext(Activity context,String buildingId,String buildingName){
		
//		AppContext appContext = (AppContext) context.getApplication();
//		appContext.setBuildingId(buildingId);
//		appContext.setBuildingName(buildingName);
//		appContext.setOrderList(null);
//		appContext.setRestaurantMap(null);
//		appContext.setRestInfoList(null);
	}
}
