/**
 * 
 */
package com.lurenshuo.zhanggui.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.lurenshuo.zhanggui.constant.SysConstant;

/**@description ϵͳ��������Ĺ�����
 * @author ������
 *
 * 2013-4-23 ����10:09:19
 */
public class NetUtils {
	/**
	 * ����û����������Ƿ��
	 * @param context
	 */
	public static int checkInternet(Activity context){
		
		ConnectivityManager connectivityManager  = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] networkInfo = connectivityManager.getAllNetworkInfo();
		if(null != networkInfo){
			
			for(int i =0;i<networkInfo.length;i++){
				
				if(networkInfo[i].getState() == NetworkInfo.State.CONNECTED){
					
					return SysConstant.NETWORK_STATUS_CONNECTED;
				}
			}
		}
		
		return SysConstant.NETWORK_STATUS_UNCONNECTED;
	}
}
