/**
 * 
 */
package com.lurenshuo.zhanggui.activity;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.lurenshuo.zhanggui.constant.SysConstant;
import com.lurenshuo.zhanggui.constant.SysURLConstant;
import com.lurenshuo.zhanggui.utils.NetUtils;
import com.lurenshuo.zhanggui.utils.ViewUtils;
import com.manager.R;

/**
 * @description 配送员登录的activity
 * @author 陈文鹏
 * 
 *         2013-4-24 下午9:48:10
 */
public class LoginActivity extends Activity {

	// 提示登录中的dlg;
	private AlertDialog dlg;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.login);

		AppContext appContext = (AppContext) LoginActivity.this
				.getApplication();
		if (appContext.getDeliver_id() != null
				&& appContext.getStatus() != null
				&& appContext.getOpen_time() != null
				&& appContext.getClose_time() != null) {
			Intent intent = new Intent();
			intent.setClass(LoginActivity.this, OrderActivity.class);
			LoginActivity.this.startActivity(intent);
			return;
		}

		// 初始化清除输入框的监听
		final ImageButton clearUserNameBtn = (ImageButton) LoginActivity.this
				.findViewById(R.id.login_clear_username);
		final ImageButton clearPasswordBtn = (ImageButton) LoginActivity.this
				.findViewById(R.id.login_clear_password);
		final EditText userNameEV = (EditText) this
				.findViewById(R.id.login_username);
		final EditText passwordEV = (EditText) this
				.findViewById(R.id.login_password);

		ViewUtils.setView_VisibleOrInVisibleListener(userNameEV,
				clearUserNameBtn);
		ViewUtils.setViewEmpty(userNameEV, clearUserNameBtn);

		ViewUtils.setView_VisibleOrInVisibleListener(passwordEV,
				clearPasswordBtn);
		ViewUtils.setViewEmpty(passwordEV, clearPasswordBtn);

		// 用户点击的登录按钮以后
		Button loginButton = (Button) this.findViewById(R.id.login_btn);
		final Handler handler = new Handler();
		loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String username = userNameEV.getText().toString().trim();
				String password = passwordEV.getText().toString().trim();

				if (!"".equals(username.trim()) && !"".equals(password.trim())) {
					// 验证用户是否可以登录，返回这个用户真正的id，如果成功就登陆
					dlg = ViewUtils
							.alertLoading(LoginActivity.this, "正在登陆....");

					new LoginThread(handler, username, password).start();
				} else {

					Toast.makeText(LoginActivity.this, "用户名或密码不能为空哦",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

	}

	/**
	 * @description 验证用户信息的线程
	 * @author 陈文鹏
	 * 
	 *         2013-4-24 下午10:54:27
	 */
	class LoginThread extends Thread {
		private Handler handler;
		private String username;
		private String password;

		public LoginThread(Handler handler, String username, String password) {
			this.handler = handler;
			this.username = username;
			this.password = password;
		}

		@Override
		public void run() {
			super.run();
			int flag = NetUtils.checkInternet(LoginActivity.this);
			// 如果网卡打开
			if (SysConstant.NETWORK_STATUS_CONNECTED == flag) {
				// 验证登录
				this.checkUserByNet(handler, username, password);

			} else {
				handler.post(new Runnable() {
					@Override
					public void run() {
						if (null != dlg) {
							dlg.dismiss();
						}
						Toast.makeText(LoginActivity.this, "您的网卡没有打开",
								Toast.LENGTH_LONG).show();
					}
				});
			}
		}

		/**
		 * @descriptions 访问网络，检查用户是否能够登陆
		 * @param handler
		 * @param username
		 * @param password
		 * @return 2013-4-25 下午6:43:59
		 */
		private String checkUserByNet(Handler handler, String username,
				String password) {
			// 访问网络，返回地址的json形式
			HttpClient httpClient = new DefaultHttpClient();

			try {
				username = java.net.URLEncoder.encode(username, "UTF-8");

				HttpPost httpPost = new HttpPost(SysURLConstant.URL_CHECK_LOGIN);
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("userName", username));
				params.add(new BasicNameValuePair("password", password));
				HttpEntity httpEntity = new UrlEncodedFormEntity(params,
						"UTF-8");
				httpPost.setEntity(httpEntity);

				// 链接超时和请求超时
				httpClient.getParams().setParameter(
						CoreConnectionPNames.CONNECTION_TIMEOUT, 20000);
				httpClient.getParams().setParameter(
						CoreConnectionPNames.SO_TIMEOUT, 20000);
				HttpResponse httpResponse = httpClient.execute(httpPost);
				// 请求正常返回
				if (SysConstant.NETWORK_RETURNSUCCESS == httpResponse
						.getStatusLine().getStatusCode()) {
					String json = EntityUtils.toString(
							httpResponse.getEntity(), "UTF-8");
					JSONObject jsonObj = new JSONObject(json);
					String result = jsonObj.getString("result");
					Integer deliver_id = jsonObj.getInt("deliver_id");
					Integer status = jsonObj.getInt("status");
					String open_time = jsonObj.getString("open_time");
					String close_time = jsonObj.getString("close_time");
					// 登陆成功就跳转
					if ("success".equals(result) && null != deliver_id) {
						if (null != dlg) {
							dlg.dismiss();
						}

						Intent intent = new Intent();
						intent.putExtra("deliver_id", deliver_id + "");
						intent.putExtra("status", status + "");
						intent.putExtra("open_time", open_time);
						intent.putExtra("close_time", close_time);
						intent.setClass(LoginActivity.this, OrderActivity.class);
						LoginActivity.this.startActivity(intent);
						// 登陆不成功提示用户
					} else {
						this.promptUser(handler, "用户名或密码错误");
					}
					// 请求未正常返回
				} else {
					this.promptUser(handler, "用户名或密码错误");
				}

			} catch (Exception e) {
				e.printStackTrace();
				this.promptUser(handler, "用户名或密码错误");
			} finally {
				if (null != httpClient) {
					httpClient.getConnectionManager().shutdown();
				}
			}

			return null;
		}

		/**
		 * @descriptions 提示用户登录状态
		 * @param handler2
		 *            2013-4-25 下午7:11:09
		 */
		private void promptUser(Handler handler, final String text) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					if (null != dlg) {
						dlg.dismiss();
					}
					Toast.makeText(LoginActivity.this, text, Toast.LENGTH_LONG)
							.show();
				}
			});
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		this.finish();
	}
}
