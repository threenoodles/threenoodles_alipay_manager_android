package com.lurenshuo.zhanggui.activity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ExpandableListActivity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.lurenshuo.zhanggui.constant.SysConstant;
import com.lurenshuo.zhanggui.constant.SysURLConstant;
import com.lurenshuo.zhanggui.utils.NetUtils;
import com.manager.R;

/**
 * @description 订单
 * @author 陈文鹏
 * 
 *         2013-4-21 下午12:01:38
 */
public class OrderActivity extends ExpandableListActivity {
	// 主线程的handler
	Handler handler = new Handler();
	// 轮询的runnable
	Runnable polling = new Runnable() {
		public void run() {
			OrderActivity.this.updateStatus(OrderActivity.this,
					SysConstant.NETWORK_STATUS_REFRESHING);
			OrderActivity.this.initData(handler);
			handler.postDelayed(polling, 180000);
		}
	};

	// 提示登录中的dlg;
	private AlertDialog dlg = null;
	private AlertDialog timeDialog = null;
	private Builder builder = null;

	private String time_last = null;
	// 配送员的id
	private String deliver_id = null;
	// 状态
	private String status = null;
	private String open_time = null;
	private String close_time = null;

	// Intent

	private Intent intent = null;
	// 父节点列表
	private List<String> parentList = new ArrayList<String>();
	// 子节点列表
	private List<List<Map<String, Object>>> orderList = new ArrayList<List<Map<String, Object>>>();
	// 订单下面的菜单详情
	private ArrayList<HashMap<String, String>> menuList = new ArrayList<HashMap<String, String>>();
	// 订单信息适配器
	private OrderAdapter orderAdapter = null;

	@Override
	protected void onRestart() {
		super.onRestart();

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.order);

		intent = this.getIntent();

		AppContext appContext = (AppContext) OrderActivity.this
				.getApplication();

		deliver_id = intent.getStringExtra("deliver_id");
		status = intent.getStringExtra("status");
		open_time = intent.getStringExtra("open_time");
		close_time = intent.getStringExtra("close_time");
		
		if (deliver_id == null || deliver_id.length() <= 0) {
			
			deliver_id = appContext.getDeliver_id();
		}else {
			appContext.setDeliver_id(deliver_id);
		}
		
		if (status == null || status.length() <= 0) {
			
			status = appContext.getStatus();
		}else {
			appContext.setStatus(status);
		}
		
		if (open_time == null || open_time.length() <= 0) {
			
			open_time = appContext.getOpen_time();
		}else {
			appContext.setOpen_time(open_time);
		}
		
		if (close_time == null || close_time.length() <= 0) {
			
			close_time = appContext.getClose_time();
		}else {
			appContext.setClose_time(close_time);
		}

//		deliver_id = intent.getStringExtra("deliver_id");
//		status = intent.getStringExtra("status");
//		open_time = intent.getStringExtra("open_time");
//		close_time = intent.getStringExtra("close_time");

		this.initListener(handler);
		orderAdapter = new OrderAdapter(this);

		this.getExpandableListView().setAdapter(orderAdapter);
		getExpandableListView().setCacheColorHint(0);

		// 更新ui状态为ing
		this.updateStatus(this, SysConstant.NETWORK_STATUS_REFRESHING);
		// 执行轮询
		this.executePolling(handler);

		// 用户点击查看一周订单
		Button viewOrdersButton = (Button) this
				.findViewById(R.id.order_history);

		viewOrdersButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {

				new Thread(new Runnable() {
					@Override
					public void run() {

						// 访问网络，返回地址的json形式
						HttpClient httpClient = new DefaultHttpClient();
						try {
							HttpPost httpPost = new HttpPost(
									SysURLConstant.URL_LIST_WEEK_ORDERS);
							List<NameValuePair> params = new ArrayList<NameValuePair>();
							params.add(new BasicNameValuePair("deliver_id",
									deliver_id));
							HttpEntity httpEntity = new UrlEncodedFormEntity(
									params, "UTF-8");
							httpPost.setEntity(httpEntity);

							// 链接超时和请求超时
							httpClient.getParams().setParameter(
									CoreConnectionPNames.CONNECTION_TIMEOUT,
									20000);
							httpClient.getParams().setParameter(
									CoreConnectionPNames.SO_TIMEOUT, 20000);
							HttpResponse httpResponse = httpClient
									.execute(httpPost);
							// 请求正常返回
							if (SysConstant.NETWORK_RETURNSUCCESS == httpResponse
									.getStatusLine().getStatusCode()) {

								final String json = EntityUtils.toString(
										httpResponse.getEntity(), "UTF-8");

								handler.post(new Runnable() {
									@Override
									public void run() {
										List<String> list = new ArrayList<String>();
										try {
											JSONArray jsonArray = new JSONArray(
													json);
											Double totalPrice = 0.0;
											BigDecimal b = null;
											for (int i = 0, n = jsonArray
													.length(); i < n; i++) {
												JSONObject json = jsonArray.getJSONObject(i);
												totalPrice = json.getDouble("totalPrice");
												b = new BigDecimal(totalPrice);
												b.setScale(2, BigDecimal.ROUND_UP);
												list.add(json.getString("day")
														+ " : "
														+ b.doubleValue()
														+ " 元");
											}

											if (list == null
												|| list.size() == 0) {
												list.add("无");
											}

											new AlertDialog.Builder(
													OrderActivity.this)
													.setTitle("最近订单总额")
													.setItems(
															list.toArray(new String[] {}),
															null)
													.setPositiveButton("确定",
															null).create()
													.show();
										} catch (Exception e) {

										}

									}
								});

							} else {

								OrderActivity.this.promptUser(handler,
										"系统正忙，请稍后重试");
							}
						} catch (Exception e) {

							OrderActivity.this
									.promptUser(handler, "系统正忙，请稍后重试");
						} finally {
							if (null != httpClient) {
								httpClient.getConnectionManager().shutdown();
							}
						}

					}
				}).start();
			}
		});

		// 是否营业
		final Button restStatusButton = (Button) this
				.findViewById(R.id.order_setCloseOpen);
		restStatusButton.setText("1".equals(status) ? "营业中" : "休息中");
		restStatusButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// 打样
				new Thread(new Runnable() {
					@Override
					public void run() {

						// 访问网络，返回地址的json形式
						HttpClient httpClient = new DefaultHttpClient();
						try {
							HttpPost httpPost = new HttpPost(
									SysURLConstant.URL_UPDATE_REST_STATUS);
							List<NameValuePair> params = new ArrayList<NameValuePair>();
							params.add(new BasicNameValuePair("deliver_id",
									deliver_id));
							params.add(new BasicNameValuePair("status", "1"
									.equals(status) ? "2" : "1"));
							HttpEntity httpEntity = new UrlEncodedFormEntity(
									params, "UTF-8");
							httpPost.setEntity(httpEntity);

							// 链接超时和请求超时
							httpClient.getParams().setParameter(
									CoreConnectionPNames.CONNECTION_TIMEOUT,
									20000);
							httpClient.getParams().setParameter(
									CoreConnectionPNames.SO_TIMEOUT, 20000);
							HttpResponse httpResponse = httpClient
									.execute(httpPost);
							// 请求正常返回
							if (SysConstant.NETWORK_RETURNSUCCESS == httpResponse
									.getStatusLine().getStatusCode()) {

								handler.post(new Runnable() {
									@Override
									public void run() {
										if ("1".equals(status)) {
											restStatusButton.setText("休息中");
											status = "2";
											Toast.makeText(OrderActivity.this,
													"打烊成功！", Toast.LENGTH_LONG)
													.show();
										} else {
											restStatusButton.setText("营业中");
											status = "1";
											Toast.makeText(OrderActivity.this,
													"营业成功！", Toast.LENGTH_LONG)
													.show();
										}
									}
								});

							} else {

								OrderActivity.this.promptUser(handler,
										"系统正忙，请稍后重试");
							}
						} catch (Exception e) {

							OrderActivity.this
									.promptUser(handler, "系统正忙，请稍后重试");
						} finally {
							if (null != httpClient) {
								httpClient.getConnectionManager().shutdown();
							}
						}

					}
				}).start();
			}
		});

		// 营业时间
		final Button order_setTimeButton = (Button) this
				.findViewById(R.id.order_setTime);

		order_setTimeButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {

				OrderActivity.this.displayOrders();
			}
		});

	}

	/**
	 * 
	 */
	public void displayOrders() {

		final List<String> times = new ArrayList<String>(2);
		times.add("营业时间：" + open_time);
		times.add("打样时间：" + close_time);

		builder = new AlertDialog.Builder(OrderActivity.this)
				.setPositiveButton("确定", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						handler.post(new Runnable() {

							@Override
							public void run() {

								// 访问网络，返回地址的json形式
								HttpClient httpClient = new DefaultHttpClient();
								try {
									HttpPost httpPost = new HttpPost(
											SysURLConstant.URL_UPDATE_REST_OPENANDCLOSETIME);
									List<NameValuePair> params = new ArrayList<NameValuePair>();
									params.add(new BasicNameValuePair(
											"deliver_id", deliver_id));
									params.add(new BasicNameValuePair(
											"open_time", open_time));
									params.add(new BasicNameValuePair(
											"close_time", close_time));
									HttpEntity httpEntity = new UrlEncodedFormEntity(
											params, "UTF-8");
									httpPost.setEntity(httpEntity);

									// 链接超时和请求超时
									httpClient
											.getParams()
											.setParameter(
													CoreConnectionPNames.CONNECTION_TIMEOUT,
													20000);
									httpClient.getParams().setParameter(
											CoreConnectionPNames.SO_TIMEOUT,
											20000);
									HttpResponse httpResponse = httpClient
											.execute(httpPost);
									// 请求正常返回
									if (SysConstant.NETWORK_RETURNSUCCESS == httpResponse
											.getStatusLine().getStatusCode()) {

										handler.post(new Runnable() {
											@Override
											public void run() {

												AppContext appContext = (AppContext) OrderActivity.this
														.getApplication();
												appContext.setOpen_time(open_time);
												appContext.setOpen_time(open_time);
												
												Toast.makeText(
														OrderActivity.this,
														"设置营业时间成功！",
														Toast.LENGTH_LONG)
														.show();
											}
										});

									} else {

										OrderActivity.this.promptUser(handler,
												"系统正忙，请稍后重试");
									}
								} catch (Exception e) {

									OrderActivity.this.promptUser(handler,
											"系统正忙，请稍后重试");
								} finally {
									if (null != httpClient) {
										httpClient.getConnectionManager()
												.shutdown();
									}
								}

							}

						});
					}
				}).setItems(times.toArray(new String[] {}),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {

								if (which == 0) {

									Calendar c = Calendar.getInstance();
									if (open_time == null
											|| open_time.length() <= 0) {
										c.set(Calendar.HOUR_OF_DAY, 10);
										c.set(Calendar.MINUTE, 0);
									} else {
										c.set(Calendar.HOUR_OF_DAY,
												Integer.valueOf(open_time
														.split(":")[0]));
										c.set(Calendar.MINUTE,
												Integer.valueOf(open_time
														.split(":")[1]));
									}

									TimePickerDialog timeView = new TimePickerDialog(
											OrderActivity.this,
											new TimePickerDialog.OnTimeSetListener() {

												@Override
												public void onTimeSet(
														TimePicker view,
														int hourOfDay,
														int minute) {

													times.clear();
													open_time = hourOfDay + ":"
															+ minute;
													times.add("营业时间："
															+ open_time);
													times.add("打烊时间："
															+ close_time);

													timeDialog.dismiss();
													OrderActivity.this
															.displayOrders();
												}
											}, c.get(Calendar.HOUR_OF_DAY), c
													.get(Calendar.MINUTE), true);

									timeView.setTitle("设置营业时间");
									timeView.show();
								} else {

									Calendar c = Calendar.getInstance();
									if (close_time == null
											|| close_time.length() <= 0) {
										c.set(Calendar.HOUR_OF_DAY, 10);
										c.set(Calendar.MINUTE, 0);
									} else {
										c.set(Calendar.HOUR_OF_DAY,
												Integer.valueOf(open_time
														.split(":")[0]));
										c.set(Calendar.MINUTE,
												Integer.valueOf(open_time
														.split(":")[1]));
									}

									TimePickerDialog timeView = new TimePickerDialog(
											OrderActivity.this,
											new TimePickerDialog.OnTimeSetListener() {

												@Override
												public void onTimeSet(
														TimePicker view,
														int hourOfDay,
														int minute) {

													times.clear();
													close_time = hourOfDay
															+ ":" + minute;
													times.add("营业时间："
															+ open_time);
													times.add("打烊时间："
															+ close_time);

													timeDialog.dismiss();
													OrderActivity.this
															.displayOrders();
												}
											}, c.get(Calendar.HOUR_OF_DAY), c
													.get(Calendar.MINUTE), true);

									timeView.setTitle("设置打样时间");
									timeView.show();

								}
							}
						});

		timeDialog = builder.create();
		timeDialog.setTitle("设置营业时间");
		timeDialog.show();

	}

	/**
	 * @descriptions 开始执行订单的轮询
	 * 
	 * @param handler
	 *            2013-4-26 下午12:12:33
	 */
	private void executePolling(final Handler handler) {
		handler.post(polling);
	}

	/**
	 * @descriptions 取消轮询
	 * 
	 * @param handler
	 *            2013-4-26 下午12:12:33
	 */
	private void cancelPolling(final Handler handler) {
		handler.removeCallbacks(polling);
	}

	/**
	 * @param handler
	 * @descriptions初始化页面的监听器
	 * 
	 *                        2013-4-26 上午9:45:32
	 */
	private void initListener(final Handler handler) {

		ImageButton refreshed = (ImageButton) this
				.findViewById(R.id.order_refreshed);
		refreshed.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				OrderActivity.this.updateStatus(OrderActivity.this,
						SysConstant.NETWORK_STATUS_REFRESHING);
				OrderActivity.this.initData(handler);
			}
		});
	}

	/**
	 * @description 订单适配器
	 * @author 陈文鹏
	 * 
	 *         2013-4-21 下午12:03:36
	 */
	class OrderAdapter extends BaseExpandableListAdapter {
		private LayoutInflater inflater;

		public OrderAdapter(Context context) {
			this.inflater = LayoutInflater.from(context);
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return orderList.get(groupPosition).get(childPosition);
		}

		@Override
		public long getChildId(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return childPosition;
		}

		@Override
		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {
			final Map<String, Object> orderData = orderList.get(groupPosition)
					.get(childPosition);
			// 设置已经配送的订单样式
			convertView = inflater.inflate(R.layout.order_item_child, null);
			RelativeLayout order = (RelativeLayout) convertView
					.findViewById(R.id.order_item);

			convertView.findViewById(R.id.order_call_receiver_container)
					.setVisibility(View.VISIBLE);
			convertView.findViewById(R.id.menu_list_container).setVisibility(
					View.VISIBLE);

			convertView.findViewById(R.id.order_call_receiver_container)
					.setVisibility(View.GONE);
			convertView.findViewById(R.id.menu_list_container).setVisibility(
					View.GONE);

			Button setDelivered_button = (Button) convertView
					.findViewById(R.id.order_setDeliveried);
			if ("5".equals(String.valueOf(orderData.get("status")))) {
				order.setBackgroundColor(getResources().getColor(R.color.gray));
				convertView.findViewById(R.id.order_icon_status).setVisibility(
						View.VISIBLE);
				// 隐藏标记为已送的按钮
				setDelivered_button.setVisibility(View.INVISIBLE);
			}else if("4".equals(String.valueOf(orderData.get("status")))){
				
				setDelivered_button.setText("标记为已取");
				order.setBackgroundColor(getResources().getColor(R.color.cooked));
			}else{
				setDelivered_button.setText("标记为已做");
				
			}
			
			
			// 用户点击了修改状态 
			setDelivered_button.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					new AlertDialog.Builder(OrderActivity.this)
							.setTitle("标记")
							.setMessage("确认要标记这条订单吗？")
							.setPositiveButton("确认",
									new DialogInterface.OnClickListener() {
										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											// 启动线程设置订单为已做
											new SetOrderDeliveredThread(
													handler, orderData,
													(String) orderData
															.get("id")).start();
										}
									}).setNegativeButton("取消", null).show();
				}
			});

			// 标题
			List<Map<String, String>> menuList = (List<Map<String, String>>) orderData
					.get("menu_list");
			String orderTitle = "";
			if (menuList != null && menuList.size() > 0) {

				for (Map<String, String> menu : menuList) {
					orderTitle += (menu.get("name") + "(" + menu.get("num") + ")");
				}
			}
			TextView restTV = (TextView) convertView
					.findViewById(R.id.order_item_rest);
			restTV.setText(orderTitle);

			TextView orderTime = (TextView) convertView
					.findViewById(R.id.order_item_time);

			// long time = Long.parseLong((String)orderData.get("time"));
			// Calendar c = Calendar.getInstance();
			// c.setTimeInMillis(time);
			String getTime = (String) orderData.get("receiver_remark");
			if (getTime != null && getTime.length() > 0) {
				orderTime.setText(getTime.substring(6, getTime.indexOf(";")));
			}

			TextView order_secretTV = (TextView) convertView
					.findViewById(R.id.order_secret);
			String secret = (String)orderData.get("secret");
			order_secretTV.setText(secret.length()<=3? "0"+secret : secret);
			// 设置价格
			TextView priceTV = (TextView) convertView
					.findViewById(R.id.order_totalPrice_text);
			priceTV.setText((String) orderData.get("menu_price"));

			// 设置地址
			// TextView addressTV =
			// (TextView)convertView.findViewById(R.id.order_receiverAddress_text);
			// addressTV.setText((String) orderData.get("receiver_address"));
			// 设置订单备注
			TextView remarkTV = (TextView) convertView
					.findViewById(R.id.order_remark_text);
			String remark = (String) orderData.get("receiver_remark");
			remarkTV.setText(remark.substring(remark.indexOf(";")+1,remark.length()));

			Button callReceiver_button = (Button) convertView
					.findViewById(R.id.order_call_receiver);
			callReceiver_button.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					String phoneNum = (String) orderData.get("receiver_phone");
					Intent intent = new Intent(Intent.ACTION_DIAL, Uri
							.parse("tel:" + phoneNum));
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
				}
			});

			return convertView;
		}

		@Override
		public int getChildrenCount(int groupPosition) {
			// TODO Auto-generated method stub
			return orderList.get(groupPosition).size();
		}

		@Override
		public Object getGroup(int groupPosition) {
			// TODO Auto-generated method stub
			return parentList.get(groupPosition);
		}

		@Override
		public int getGroupCount() {
			// TODO Auto-generated method stub
			return parentList.size();
		}

		@Override
		public long getGroupId(int groupPosition) {
			// TODO Auto-generated method stub
			return groupPosition;
		}

		@Override
		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {

			convertView = inflater.inflate(R.layout.order_item_parent, null);

			TextView orderTV = (TextView) convertView
					.findViewById(R.id.order_item_parent_text);
			orderTV.setText(parentList.get(groupPosition));

			ImageView indicatorIV = (ImageView) convertView
					.findViewById(R.id.order_item_indicator);
			if (isExpanded) {

				indicatorIV.setImageDrawable(OrderActivity.this.getResources()
						.getDrawable(R.drawable.order_icon_contracted));
			} else {

				indicatorIV.setImageDrawable(OrderActivity.this.getResources()
						.getDrawable(R.drawable.order_icon_expanded));
			}

			Button order_num = (Button) convertView
					.findViewById(R.id.order_num);
			if (null != orderList.get(groupPosition)) {
				order_num.setText(orderList.get(groupPosition).size() + "");
			} else {
				order_num.setText("0");
			}
			return convertView;
		}

		@Override
		public boolean hasStableIds() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return true;
		}
	}

	// 用户点击了某一个订单以后（listView里面嵌套了listview，如果子listview是点击父级的listview动态生成的，那么必须在点击事件中动态生成子级的listview，否则会数据错乱）
	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		// 收起或者放下
		if (v.findViewById(R.id.order_call_receiver_container).getVisibility() == View.GONE) {
			// 更新订单对应的菜单信息
			menuList = (ArrayList<HashMap<String, String>>) orderList
					.get(groupPosition).get(childPosition).get("menu_list");

			ListView menuLV = (ListView) v.findViewById(R.id.menu_list);
			MenuAdapter menuAdapter = new MenuAdapter(OrderActivity.this);
			menuLV.setAdapter(menuAdapter);

			// 动态设置一下listview的高度
			ListAdapter listAdapter = menuLV.getAdapter();
			if (null != listAdapter) {
				int totalHeight = 0;
				for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
					// listAdapter.getCount()返回数据项的数目
					View listItem = listAdapter.getView(i, null, menuLV);
					listItem.measure(0, 0); // 计算子项View 的宽高
					totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
				}
				ViewGroup.LayoutParams params = menuLV.getLayoutParams();
				params.height = totalHeight
						+ (menuLV.getDividerHeight() * (listAdapter.getCount() - 1));
				// listView.getDividerHeight()获取子项间分隔符占用的高度
				// params.height最后得到整个ListView完整显示需要的高度
				menuLV.setLayoutParams(params);
			}
			v.findViewById(R.id.order_call_receiver_container).setVisibility(
					View.VISIBLE);
			v.findViewById(R.id.menu_list_container)
					.setVisibility(View.VISIBLE);
		} else {

			v.findViewById(R.id.order_call_receiver_container).setVisibility(
					View.GONE);
			v.findViewById(R.id.menu_list_container).setVisibility(View.GONE);
		}
		return false;
	}

	// 订单中的菜单的适配器
	class MenuAdapter extends BaseAdapter {
		private LayoutInflater inflater;

		public MenuAdapter(Context context) {

			this.inflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {

			return menuList.size();
		}

		@Override
		public Object getItem(int position) {

			return menuList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			HashMap<String, String> menu = menuList.get(position);

			convertView = inflater.inflate(R.layout.order_menu, null);
			TextView menuNameTV = (TextView) convertView
					.findViewById(R.id.order_menu_name);
			TextView menuPriceTv = (TextView) convertView
					.findViewById(R.id.order_menu_price);
			TextView menuNumTv = (TextView) convertView
					.findViewById(R.id.order_menu_num);

			menuNameTV.setText(menu.get("name"));
			menuPriceTv.setText("￥" + menu.get("price"));
			menuNumTv.setText(menu.get("num"));

			return convertView;
		}
	}

	/**
	 * @param handler
	 * @descriptions 初始化数据
	 * 
	 *               2013-4-21 下午12:17:36
	 */
	public void initData(Handler handler) {

		int flag = NetUtils.checkInternet(OrderActivity.this);
		// 如果网卡打开了
		if (SysConstant.NETWORK_STATUS_CONNECTED == flag) {
			new OrderThread(deliver_id, handler).start();
		} else {
			handler.post(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(OrderActivity.this, "您的网卡没有打开",
							Toast.LENGTH_LONG).show();
					OrderActivity.this.updateStatus(OrderActivity.this,
							SysConstant.NETWORK_STATUS_REFRESHED);
				}
			});
		}
	}

	/**
	 * @description 设置用户订单已经配送的线程
	 * @author 陈文鹏
	 * 
	 *         2013-4-26 下午2:03:49
	 */
	class SetOrderDeliveredThread extends Thread {
		private Handler handler;
		private String order_id;
		private Map<String, Object> orderData;

		public SetOrderDeliveredThread(Handler handler,
				Map<String, Object> orderData, String order_id) {
			this.handler = handler;
			this.order_id = order_id;
			this.orderData = orderData;
		}

		@Override
		public void run() {
			super.run();
			String result = this.setOrderDeliveredByNet(order_id);
			if (null != result && "\"success\"".equals(result)) {
				handler.post(new Runnable() {
					@Override
					public void run() {
						if (null != orderAdapter) {
							OrderActivity.this.promptUser(handler, "修改成功!");
							if(orderData.get("status") == null || Integer.valueOf(String.valueOf(orderData.get("status")))<=3 ){
								orderData.put("status", 4);
							}else{
								orderData.put("status", 5);
							}

							orderAdapter.notifyDataSetChanged();
						}
					}
				});
			}

			// 更新状态
			handler.post(new Runnable() {
				@Override
				public void run() {
					OrderActivity.this.updateStatus(OrderActivity.this,
							SysConstant.NETWORK_STATUS_REFRESHED);
				}
			});

		}

		/**
		 * @descriptions 访问网络设置这个订单为已经配送
		 * 
		 * @param deliver_id
		 * @param time_last
		 * @return 2013-4-26 下午2:00:04
		 */
		public String setOrderDeliveredByNet(String orderId) {
			// 访问网络，返回地址的json形式
			HttpClient httpClient = new DefaultHttpClient();
			try {
				HttpPost httpPost = new HttpPost(
						SysURLConstant.URL_SETORDERDELIVERED);
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("order_id", orderId));
				HttpEntity httpEntity = new UrlEncodedFormEntity(params,
						"UTF-8");
				httpPost.setEntity(httpEntity);

				// 链接超时和请求超时
				httpClient.getParams().setParameter(
						CoreConnectionPNames.CONNECTION_TIMEOUT, 20000);
				httpClient.getParams().setParameter(
						CoreConnectionPNames.SO_TIMEOUT, 20000);
				HttpResponse httpResponse = httpClient.execute(httpPost);
				// 请求正常返回
				if (SysConstant.NETWORK_RETURNSUCCESS == httpResponse
						.getStatusLine().getStatusCode()) {

					String json = EntityUtils.toString(
							httpResponse.getEntity(), "UTF-8");
					return json;
				} else {

					OrderActivity.this.promptUser(handler, "系统正忙，请稍后重试");
				}
			} catch (Exception e) {

				OrderActivity.this.promptUser(handler, "系统正忙，请稍后重试");
			} finally {
				if (null != httpClient) {
					httpClient.getConnectionManager().shutdown();
				}
			}
			return null;
		}
	}

	/**
	 * @description 访问网络，加载送餐员最新订单的线程
	 * @author 陈文鹏
	 * 
	 *         2013-4-26 下午8:50:00
	 */
	class OrderThread extends Thread {
		private String deliver_id;
		private Handler handler;

		public OrderThread(String deliver_id, Handler handler) {
			this.deliver_id = deliver_id;
			this.handler = handler;
		}

		@Override
		public void run() {
			super.run();
			String result = this.listOrderByNet(deliver_id, time_last);
			// 解析订单的json
			List<Map<String, Object>> orderList_new = this
					.parseOrderJson(result);

			// 如果有新订单了才会把订单重新分组
			if (orderList_new.size() > 0) {
				parentList = new ArrayList<String>();
//				parentList.add("历史订单");
				parentList.add("最新订单");

				List<List<Map<String, Object>>> orderList_ = new ArrayList<List<Map<String, Object>>>();
				List<Map<String, Object>> _orderList = new ArrayList<Map<String, Object>>();
				for (List<Map<String, Object>> orders : orderList) {
					for (Map<String, Object> order : orders) {
						_orderList.add(order);
					}
				}
				_orderList.addAll(orderList_new);
				orderList_.add(_orderList);

//				final SimpleDateFormat dateFormat = new SimpleDateFormat(
//						"HH 时 mm 分");

				Collections.sort(_orderList,
						new Comparator<Map<String, Object>>() {
							@Override
							public int compare(Map<String, Object> arg0,
									Map<String, Object> arg1) {
								try {
									String time0 = (String) arg0
											.get("receiver_remark");
									time0 = time0.substring(5, 15);
									int time0Hour = Integer.parseInt(time0
											.substring(0, time0.indexOf("时"))
											.trim());
									int time0Minute = Integer.parseInt(time0
											.substring(time0.indexOf("时") + 1,
													time0.indexOf("分")).trim());

									String time1 = (String) arg1
											.get("receiver_remark");
									time1 = time1.substring(6, 15);
									int time1Hour = Integer.parseInt(time1
											.substring(0, time1.indexOf("时"))
											.trim());
									int time1Minute = Integer.parseInt(time1
											.substring(time1.indexOf("时") + 1,
													time1.indexOf("分")).trim());

									return (int) (time0Hour * 60 + time0Minute
											- time1Hour * 60 - time1Minute);

								} catch (Exception e) {
									e.printStackTrace();
									return 0;
								}
							}
						});
				Collections.sort(orderList_new,
						new Comparator<Map<String, Object>>() {
							@Override
							public int compare(Map<String, Object> arg0,
									Map<String, Object> arg1) {
								try {
									String time0 = (String) arg0
											.get("receiver_remark");
									time0 = time0.substring(5, 15);
									int time0Hour = Integer.parseInt(time0
											.substring(0, time0.indexOf("时"))
											.trim());
									int time0Minute = Integer.parseInt(time0
											.substring(time0.indexOf("时") + 1,
													time0.indexOf("分")).trim());

									String time1 = (String) arg1
											.get("receiver_remark");
									time1 = time1.substring(6, 15);
									int time1Hour = Integer.parseInt(time1
											.substring(0, time1.indexOf("时"))
											.trim());
									int time1Minute = Integer.parseInt(time1
											.substring(time1.indexOf("时") + 1,
													time1.indexOf("分")).trim());

									return (int) (time0Hour * 60 + time0Minute
											- time1Hour * 60 - time1Minute);

								} catch (Exception e) {
									e.printStackTrace();
									return 0;
								}
							}
						});

				orderList = orderList_;
				//
				Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				long[] pattern = { 100, 1500, 100, 1200, 100, 1000 }; // 停止 开启
																		// 停止 开启
				vibrator.vibrate(pattern, -1); // 重复两次上面的pattern
												// 如果只想震动一次，index设为-1
				//
				RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
				MediaPlayer mp = new MediaPlayer();
				try {
					mp.setDataSource(OrderActivity.this, RingtoneManager
							.getDefaultUri(RingtoneManager.TYPE_ALARM));
					mp.prepare();
					mp.start();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// 启动
				intent.putExtra("deliver_id", deliver_id + "");
				intent.putExtra("status", status + "");
				intent.putExtra("open_time", open_time);
				intent.putExtra("close_time", close_time);

				intent.setClass(OrderActivity.this, OrderService.class);
				OrderActivity.this.startService(intent);
			}

			// 更新订单信息
			handler.post(new Runnable() {
				@Override
				public void run() {
					if (null != orderAdapter) {
						// 更新状态
						OrderActivity.this.updateStatus(OrderActivity.this,
								SysConstant.NETWORK_STATUS_REFRESHED);
						orderAdapter.notifyDataSetChanged();

					}
				}
			});
		}

		/**
		 * @descriptions 解析orderjson，并赋值
		 * 
		 * @param result
		 *            2013-4-26 下午9:24:01
		 */
		private List<Map<String, Object>> parseOrderJson(String result) {
			List<Map<String, Object>> orderList_new = new ArrayList<Map<String, Object>>();
			try {
				if (null == result || "".equals(result)) {
					return orderList_new;
				}

				JSONObject jsonObject = new JSONObject(result);
				String status = jsonObject.getString("status");
				if ("noLogin".equals(status)) {
					// 取消轮询
					OrderActivity.this.cancelPolling(handler);
					// 未知原因导致用户的deliver_id丢失，直接返回登录页面
					Intent intent = new Intent();
					intent.setClass(OrderActivity.this, LoginActivity.class);
					OrderActivity.this.startActivity(intent);
					return orderList_new;
				}
				time_last = jsonObject.getString("time_last");
				JSONArray orderList_ = jsonObject.getJSONArray("order_list");

				Log.v("tag", time_last);

				for (int i = 0; i < orderList_.length(); i++) {
					Map<String, Object> order = new HashMap<String, Object>();

					JSONObject order_json = orderList_.getJSONObject(i);

					order.put("id", order_json.getString("id"));
					order.put("rest_name", order_json.getString("rest_name"));
					order.put("carriage", order_json.getString("carriage"));
					order.put("menu_price", order_json.getString("menu_price"));
					order.put("receiver_phone",
							order_json.getString("receiver_phone"));
					order.put("time", order_json.getString("ordered_time"));
					order.put("receiver_address",
							order_json.getString("address"));
					order.put("receiver_remark", order_json.getString("remark"));
					order.put("secret", order_json.getString("secret"));
					order.put("status", order_json.getString("status"));

					List<Map<String, String>> menuList = new ArrayList<Map<String, String>>();
					JSONArray menuList_ = order_json.getJSONArray("menu_list");

					for (int j = 0; j < menuList_.length(); j++) {
						JSONObject menu_ = menuList_.getJSONObject(j);
						Map<String, String> menu = new HashMap<String, String>();
						menu.put("name", menu_.getString("name"));
						menu.put("num", menu_.getString("count"));
						menu.put("price", menu_.getString("price"));

						menuList.add(menu);
					}

					order.put("menu_list", menuList);
					orderList_new.add(order);

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return orderList_new;
		}

		/**
		 * @descriptions 访问网络返回用户最新的订单
		 * 
		 * @param deliver_id2
		 * @return 2013-4-26 下午9:12:35
		 */
		private String listOrderByNet(String deliver_id, String time_last) {
			// 访问网络，返回地址的json形式
			HttpClient httpClient = new DefaultHttpClient();
			try {
				HttpPost httpPost = new HttpPost(SysURLConstant.URL_LIST_ORDER);
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("deliver_id", deliver_id));
				params.add(new BasicNameValuePair("time_last", time_last));
				HttpEntity httpEntity = new UrlEncodedFormEntity(params,
						"UTF-8");
				httpPost.setEntity(httpEntity);

				// 链接超时和请求超时
				httpClient.getParams().setParameter(
						CoreConnectionPNames.CONNECTION_TIMEOUT, 20000);
				httpClient.getParams().setParameter(
						CoreConnectionPNames.SO_TIMEOUT, 20000);
				HttpResponse httpResponse = httpClient.execute(httpPost);
				// 请求正常返回
				if (SysConstant.NETWORK_RETURNSUCCESS == httpResponse
						.getStatusLine().getStatusCode()) {

					String json = EntityUtils.toString(
							httpResponse.getEntity(), "UTF-8");
					return json;
				} else {

					OrderActivity.this.promptUser(handler, "系统正忙，请稍后重试");
				}
			} catch (Exception e) {

				OrderActivity.this.promptUser(handler, "系统正忙，请稍后重试");
			} finally {
				if (null != httpClient) {
					httpClient.getConnectionManager().shutdown();
				}
			}
			return null;
		}

	}

	/**
	 * @descriptions 提示用户登录状态
	 * @param handler2
	 *            2013-4-25 下午7:11:09
	 */
	private void promptUser(Handler handler, final String text) {
		handler.post(new Runnable() {
			@Override
			public void run() {
				if (null != dlg) {
					dlg.dismiss();
				}
				Toast.makeText(OrderActivity.this, text, Toast.LENGTH_LONG)
						.show();
			}
		});
	}

	/**
	 * @descriptions更新UI
	 * @param context
	 * @param status
	 *            2013-4-26 下午10:14:47
	 */
	private void updateStatus(OrderActivity context, Integer status) {
		ImageButton image_refresh = (ImageButton) context
				.findViewById(R.id.order_refreshed);
		ProgressBar image_refreshing = (ProgressBar) context
				.findViewById(R.id.order_refreshing);

		if (status == SysConstant.NETWORK_STATUS_REFRESHING) {
			image_refresh.setVisibility(View.GONE);
			image_refreshing.setVisibility(View.VISIBLE);
		} else {

			image_refresh.setVisibility(View.VISIBLE);
			image_refreshing.setVisibility(View.GONE);
		}
	}

	// 如果用户在主页点击了back就提示他退出
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			new AlertDialog.Builder(OrderActivity.this)
					.setTitle("退出")
					.setMessage("确认要退出路人说管理软件？")
					.setPositiveButton("确认",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {

									OrderActivity.this.finish();
									handler.removeCallbacks(polling);
								}
							}).setNegativeButton("取消", null).show();

		}
		return super.onKeyDown(keyCode, event);
	}
	
	// 用户切换以后，就销毁掉这个activity
	@Override
	protected void onPause() {
		super.onPause();
		
	}
}
