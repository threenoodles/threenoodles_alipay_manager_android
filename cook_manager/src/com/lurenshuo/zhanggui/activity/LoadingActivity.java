package com.lurenshuo.zhanggui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.manager.R;

public class LoadingActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.setContentView(R.layout.cook_loading_onstart);
		new Handler().postDelayed(new Runnable() {
			public void run() {
				Intent i = new Intent(LoadingActivity.this,
						LoginActivity.class);
				LoadingActivity.this.startActivity(i);
			}
		}, 2000);
	}

	@Override
	protected void onPause() {
		super.onPause();
		this.finish();
	}
}
