package com.lurenshuo.zhanggui.activity;

import android.R;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class OrderService extends Service {

	// 配送员的id
	private String deliver_id = null;
	// 状态
	private String status = null;
	private String open_time = null;
	private String close_time = null;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {

	};

	@Override
	public void onStart(Intent intent, int startId) {

	};

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		Notification notification = new Notification(R.drawable.ic_input_add,
				"您有新订单", System.currentTimeMillis());
//		Intent notificationIntent = new Intent(this, OrderActivity.class);
		
		intent.setClass(this, OrderActivity.class);
		//单例启动
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		intent.putExtra("deliver_id", deliver_id);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				intent, 0);
		notification.setLatestEventInfo(this, "您有新订单", "点击查看新订单（路人说）", pendingIntent);
		startForeground(98, notification);
		
		
		return START_NOT_STICKY;
	}

}
